import {AppRegistry} from 'react-native';
import {name as appName} from './app.json'; //fetching app name

const React = require('react-native');      //React-Native library for cross platform 
const Firebase = require('firebase');       //Firebase to save your ToDos

//Getting all the components to build the GUI
var {
    Component,
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableHighLight,
    TextInput,
    ListView
} = React;

//Application class
class Todo extends Component{
    //constructor to initialize firebases
    constructor(props){
        super(props);
        var myfirebaseRef = new Firebase('url');

        this.itemRef = myfirebaseRef.child('items');
        
        //Initializing the state of todo
        this.state = {
            newTodo:'',
            todoSource: new ListView.DataSource({rowHasChanged: (row1,row2) => row1 != row2})
        };

        this.items = [];

    }
    
    //Adiing and Deletion of the tasks
    componentDismount(){
        //Adding an item to the list in firebase
        this.itemRef.on('child_added',(dataSnapshot) => {
            this.items.push({id: dataSnapshot.key(),text:dataSnapshot.val()});
            this.setState({
                todoSource: this.state.todoSource.cloneWithRows(this.items)
            });
        });
        
        //Removing an item from the list in firebase
        this.itemRef.on('child_removed',(dataSnapshot) => {
            this.items = this.items.filter((x) => x.id != dataSnapshot.key());
            this.setState({
                todoSource: this.state.todoSource.cloneWithRows(this.items)
            });
        });
    }

    //Adding a new ToDo item
    addTodo(){
        if(this.state.newTodo != ''){
            this.itemRef.push({
                todo: this.this.state.newTodo
            });
            this.setState({
                newTodo: ''
            });
        }
    }
    
    //Removing Todo item
    removeTodo(rowData){
        this.itemRef.child(rowData.id()).remove();
    }
    
    //Rendering the todo lists
    renderRow(rowData){
       return(
          <TouchableHighLight
          underlayColor = '#dddddd'
          onPress = {() => this.removeTodo(rowData)}>
            <view>
                <view style = {styles.row}>
                    <text style = {styles.todoText}>
                        {rowData.text}            
                    </text>
                </view>
                <view style = {styles.seperator} />
            </view>
          </TouchableHighLight>
       ) 
    }
    
    //Final render function
    render(){
      return (
        <view style = {styles.appContainer}>
            <view style = {styles.titleView}>
                <Text style = {styles.titleText}>
                    My Todos
                </Text>
            </view>

            <view style = {styles.inputContainer}>
                <TextInput style = {styles.input} onChangeText = {(text) => this.setState({newTodo: text})} value ={this.state.newTodo}/>
                <TouchableHighLight
                    style = {styles.button}
                    onPress = {() => this.addTodo()}
                    underlayColor='#dddddd'>

                <Text style={styles.btnText}>
                    ADD
                </Text>

                </TouchableHighLight>
            </view>
            <ListView 
                dataSource = {this.state.todoSource}
                renderRow = {this.renderRow.bind(this)} />        
            </view>
        )
    }
}

//Stylesheet that stores the values for each of the rendering tags
//Easy manipulation of colour and dimension of each components
var styles = StyleSheet.create({
    appContainer:{
      flex: 1
    },
    titleView:{
      backgroundColor: '#48afdb',
      paddingTop: 30,
      paddingBottom: 10,
      flexDirection: 'row'
    },
    titleText:{
      color: '#fff',
      textAlign: 'center',
      fontWeight: 'bold',
      flex: 1,
      fontSize: 20,
    },
    inputcontainer: {
      marginTop: 5,
      padding: 10,
      flexDirection: 'row'
    },
    button: {
      height: 36,
      flex: 2,
      flexDirection: 'row',
      backgroundColor: '#48afdb',
      justifyContent: 'center',
      color: '#FFFFFF',
      borderRadius: 4,
    },
    btnText: {
      fontSize: 18,
      color: '#fff',
      marginTop: 6,
    },
    input: {
      height: 36,
      padding: 4,
      marginRight: 5,
      flex: 4,
      fontSize: 18,
      borderWidth: 1,
      borderColor: '#48afdb',
      borderRadius: 4,
      color: '#48BBEC'
    },
    row: {
      flexDirection: 'row',
      padding: 12,
      height: 44
    },
    separator: {
      height: 1,
      backgroundColor: '#CCCCCC',
    },
    todoText: {
      flex: 1,
    }
});

//Registering the app components
AppRegistry.registerComponent(appName, () => ToDo);
