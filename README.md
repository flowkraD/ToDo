# ToDo-IOS
A simple ToDo App based on react-native
### Screenshot
Disclaimer: The Screenshot may not be updated very often so you may be seeing a view of the previous commit of the app
<br />
![alt text](https://i.imgur.com/gDHAinm.png)
### Operation
Type in your ToDo and click Add.
Once you are done press on the ToDo and it will automatically disappear from the list.
### Contributions
Suggestions and Codebacks are welcome.
# Cheers!
